/*
Author: Luis Fueris

Awesome-name project extracts MTAs servers from mail headers and
analyzes them in order to check phishing sites. It also search IOCs within
mail body (not yet)

*/

package main

import (
	"fmt"
	"os"
	"strings"
	"net"
	"sync"

	// remote packages
	cmdr "github.com/hedzr/cmdr"

	// local packages
	auth "heanco/local/auth"
	global "heanco/local/global"
	sanitize "heanco/local/sanitize"
	scan "heanco/local/scan"
	abuseip "heanco/local/abuseip"
)

// Main function
func main() {
	//var mtas []global.Node

	rootCmd := sanitize.BuildRootCmd()
	err := cmdr.Exec(rootCmd,
		// Disable internal builtin commands (help, verbose, generate...)
		cmdr.WithBuiltinCommands(false, false, false, false, false))
	if err != nil {
		os.Exit(-1)
		panic(err)
	}

	// Check if file exists
	sampleFile := cmdr.GetStringR("sample-file")
	info, err := os.Stat(sampleFile)
	if os.IsNotExist(err) {
		fmt.Println("\nSample does not exists!")
		os.Exit(-1)
		return
	}

	if info.IsDir() {
		fmt.Println("\nSample argument must be a file!")
		os.Exit(-1)
		return
	}

	// Maybe this action could be on sanitize.go (NewSubCommand -> Action)
	subCommands := rootCmd.GetSubCommandNamesBy(" ")
	if strings.Split(subCommands, " ")[1] == string(global.SCAN) {
		var wg sync.WaitGroup
		var mutex sync.Mutex
		// Extract MTAs from header
		fmt.Println("[*] Scan action has been triggered!")
		mtas, _ := scan.HeaderInspection(sampleFile)
		
		// Read configuration file
		confVars := auth.ParseConfFile()
		abuseipToken := confVars[0]
		if len(abuseipToken) == 0 {
			fmt.Println("[!] Write your AbuseIP API token in conf/api.conf")
			os.Exit(-1)
		}
		
		// AbuseIP feed
		len := len(mtas)
		wg.Add(len)
		for _, mta := range mtas {
			go func(mta global.Node, abuseipToken string, mutex sync.Mutex) {
				defer wg.Done()
				if mta.Key == global.SENDER &&
					!(mta.Ip.Equal(net.IPv4(0, 0, 0, 0))) {
						abuseip.CheckIP(abuseipToken, mta.Host, 
										mta.Ip.String(), mutex)
					}
			}(mta, abuseipToken, mutex)
		}

		wg.Wait()
	}

	os.Exit(0)
	return
}