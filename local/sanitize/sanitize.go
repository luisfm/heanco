/*
Package implements input validation for awesome-name project.

Execution syntax is as following :
./<awesome-name> -s <sample-mail>

-s option specifies mail sample that needs to be analyzed
*/
package sanitize

import (
	"fmt"

	cmdr "github.com/hedzr/cmdr"
)

const (
	versionName = "1.0"
	appName     = "heanco"
)

// BuildRootCmd is a public function that builds command line arguments,
// a *cmdr.RootCommand object is returned with there options.
func BuildRootCmd() (rootCmd *cmdr.RootCommand) {

	root := cmdr.Root(appName, versionName).
		PreAction(sanitizePreAction).
		PostAction(sanitizePostAction)

	root.NewSubCommand().
		Titles("scan", "s").
		Description("Scan sample", "").
		Action(func(cmd *cmdr.Command, args []string) (err error) {
			return
		})

	root.NewFlagV("").
		Titles("sample-file", "s").
		Description("Sample mail file", "").
		Placeholder("").
		OnSet(func(keyPath string, value interface{}) {
			fmt.Println("[*] Sample file", value)
			return
		})

	rootCmd = root.RootCommand()

	return rootCmd
}

// sanitizePreAction runs before sanitize arguments
//
func sanitizePreAction(cmd *cmdr.Command, args []string) (err error) {
	// Do nothing
	return
}

// sanitizePostAction runs after sanitize arguments
//
func sanitizePostAction(cmd *cmdr.Command, args []string) {
	// Do nothing
	return
}
