/*
Package const specifies global vars and some stuff

*/

package global

import "net"

type PossibleCommand string

const (
	SCAN PossibleCommand = "scan"
)

type RegularExpression string

const (
	FROM        	RegularExpression = "from[[:blank:]]"
	BY          	RegularExpression = ".*by" 
	ENDOFHEADER 	RegularExpression = "From:[[:blank:]]"
	SPF 			RegularExpression = "spf="
	CLIENTIP		RegularExpression = "client-ip="
	SMTPMAILFROM 	RegularExpression = "smtp.mailfrom="
)

type Node struct {
	Key  string
	Host string
	Ip   net.IP
}

const (
	SENDER      string = "Sender"
	INTERNAL_IP string = "Internal IP"
)

const (
	CONF   string = "conf/api.conf"
	OTXVAR string = "X_OTX_API_KEY"
)

const (
	HEADERAPP 	string = "application/json"
	DAYS 		string = "90"
	URLABUSE 	string = "https://api.abuseipdb.com/api/v2/check"
)