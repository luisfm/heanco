/*
Package auth performs authentication via api.conf

*/
package auth

import (
	"bufio"
	"log"
	"os"
	"strings"

	// local packages
	global "heanco/local/global"
)

// readConfFile reads global.CONF file and extracts configuration variables
//
func readConfFile() []string {
	file, err := os.Open(global.CONF)
	if err != nil {
		log.Panic(err)
	}
	defer file.Close()

	var confVars []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if len(line) > 0 && line[0] != '#' {
			split := strings.Split(line, ":")
			confVars = append(confVars, split[1])
		}
	}

	return confVars
}

// ParseConfFile reads configuration file and create bot instance with specific
// token
func ParseConfFile() []string {
	confVars := readConfFile()
	return confVars
}
