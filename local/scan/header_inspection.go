/*
Package scan implements header inspection and abuse domain. In this
case, the file extracts MTAs from mail header

*/
package scan

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"regexp"
	"strings"
	"sync"

	// Local packages
	global "heanco/local/global"
)

// buildFlowGraph builds graph that shows MTAs flows
//
func buildFlowGraph(mtas []global.Node) {
	fmt.Println("[+] Building flow graph of MTAs nodes")

	nNode := 0
	for i := len(mtas); i > 0; i = i - 2 {
		fmt.Printf("\t[%d] %s (%s) -> %s (%s)\n", nNode,
			mtas[i-2].Host, mtas[i-2].Ip,
			mtas[i-1].Host, mtas[i-1].Ip)
		if i != 2 {
			fmt.Printf("\t|\n")
		}
		nNode++
	}
	fmt.Printf("\n")

	return
}

// dnsLookUp performs DNS domain resolution
//
func dnsLookUp(mtas []global.Node) {
	var wg sync.WaitGroup
	len := len(mtas)
	wg.Add(len)

	for i, mta := range mtas {
		go func(i int, mta global.Node) {
			defer wg.Done()
			ips, err := net.LookupIP(mta.Host)
			if err != nil {
				mtas[i].Ip = net.IPv4(0, 0, 0, 0)
				return
			}

			for _, ip := range ips {
				// Maybe we need to change it if we have more than one ip
				mtas[i].Ip = ip
			}
		}(i, mta)
	}

	wg.Wait()

	return
}

// parseMtas extracts MTAs domain/IP and sender from sample file
//
func parseMtas(file *os.File) ([]global.Node, string) {
	var mtas []global.Node
	var sender string
	var spf string
	var client string
	var fromDomain string

	mailFrom := regexp.MustCompile(
		string(global.SMTPMAILFROM))
	clientIP:= regexp.MustCompile(
		string(global.CLIENTIP))
	spfResult := regexp.MustCompile(
		string(global.SPF))
	fromRe := regexp.MustCompile(
		string(global.FROM))
	byRe := regexp.MustCompile(
		string(global.BY))
	endOfHeaderRe := regexp.MustCompile(
		string(global.ENDOFHEADER))
	reader := bufio.NewReader(file)
	for {
		bytes, err := reader.ReadBytes(' ')
		if err != nil {
			// End of sample file
			break
		}

		if endOfHeaderRe.Match(bytes) {
			// End of headers ("From:"" string), we cannot extract
			// anymore MTAs
			bytes, _ = reader.ReadBytes('\n')
			sender = string(bytes)
			fmt.Printf("[!] Sender name/mail: %s", sender)
			break
		} else if fromRe.Match(bytes) {
			// Okay, next word must be a MTA sender node
			bytes, _ = reader.ReadBytes(' ')
			mtas = append(mtas, global.Node{Key: "Sender",
				Host: strings.TrimSpace(
					string(bytes))})
		} else if byRe.Match(bytes) {
			// Okay, next word must be a MTA recipient node
			bytes, _ = reader.ReadBytes(' ')
			mtas = append(mtas, global.Node{Key: "Recipient",
				Host: strings.TrimSpace(
					string(bytes))})
		} else if spfResult.Match(bytes) {
			// Extract SPF result
			spf = strings.Split(string(bytes), "=")[1]
		} else if mailFrom.Match(bytes) {
			// Extract MAIL FROM for SPF result
			_fromDomain := strings.Split(string(bytes), "=")[1]
			fromDomain = strings.TrimSpace(_fromDomain)
		} else if clientIP.Match(bytes) {
			// Extract client IP for SPF result
			_client := strings.Split(string(bytes), "=")[1]
			client = strings.TrimSpace(_client)
			fmt.Printf("[!] SPF for IP %s with MAIL FROM %s -> %s\n", 
										strings.TrimSuffix(client, ";"),
										strings.TrimSuffix(fromDomain, ";"),
										spf) 
		}
	}
	// Return MTA datastructure
	return mtas, sender
}

// HeaderInspection is a public function that extracts, parses and sorts MTAs
//
func HeaderInspection(sampleFile string) ([]global.Node, string) {
	var mtas []global.Node
	var sender string

	file, err := os.Open(sampleFile)
	if err != nil {
		os.Exit(-1)
		panic(err)
	}
	defer file.Close()

	fmt.Println("[+] Extracting MTAs from sample header")
	mtas, sender = parseMtas(file)
	if len(mtas) == 0 {
		fmt.Println("[!] Sample has not MTAs headers...")
		os.Exit(-1)
		return nil, ""
	}

	dnsLookUp(mtas)
	buildFlowGraph(mtas)

	return mtas, sender
}
