/*
Package abuseip performs requests to abuseipdb API

*/
package abuseip

import (
	"fmt"
	"os"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"strings"
	"strconv"
	"sync"

	// local packages
	global "heanco/local/global"
)


// doAbuseRiskClassification performs a string scoring according
// to scoring variable
func doAbuseRiskClassification(scoring float64) string {
	if (scoring == 0) {
		return "None"
	} else if (scoring > 0 && scoring < 50) {
		return "Low"
	} else if (scoring >= 50 && scoring < 70) {
		return "Medium"
	} else if (scoring >= 70 && scoring <= 100) {
		return "High"
	}

	return "Unknown"
}


// pareAbuseJson extracts valuable fields from JSON response
//
func parseAbuseJson(body []byte) (float64, string, string, string) {
	var fields map[string]interface{}

	json.Unmarshal([]byte(body), &fields)	
	data := fields["data"].(map[string]interface{})
	scoring := data["abuseConfidenceScore"].(float64)
	country := data["countryCode"].(string)
	domain := data["domain"].(string)
	isp := data["isp"].(string)

	return scoring, country, domain, isp
}


// ParseConfFile reads configuration file and create bot instance with specific
// token
func CheckIP(abuseipToken string, host string, ip string, mutex sync.Mutex) string {

	client := &http.Client{}
	req, err := http.NewRequest("GET", global.URLABUSE, nil)
	if err != nil {
		fmt.Println("Error building new GET request")
		os.Exit(-1)
	}
	req.Header.Add("Accept", global.HEADERAPP)
	req.Header.Add("Key", abuseipToken)
	// Add params
	q := req.URL.Query()
	q.Add("ipAddress", ip)
	q.Add("maxAgeInDays", global.DAYS)

	req.URL.RawQuery = q.Encode()
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Errored when sending request to the server")
		os.Exit(-1)
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)
	code, _ := strconv.Atoi(strings.Split(resp.Status, " ")[0])
	var risk string
	if code == 200 {
		scoring, country, domain, isp := parseAbuseJson([]byte(body))
		risk = doAbuseRiskClassification(scoring)
		// Critical section that prints the messages orderly
		mutex.Lock() 
		{
			fmt.Printf("[*] Checking %s (%s) reputation\n", host, ip)
			fmt.Printf("[!] %s (%s)\n\t", host, ip)
			fmt.Printf("[!] Risk: %s (%f)\n\t", risk, scoring)
			fmt.Printf("[!] Country: %s\n\t", country)
			fmt.Printf("[!] Domain: %s\n\t", domain)
			fmt.Printf("[!] ISP: %s\n", isp)
		}
		mutex.Unlock() 
	} else {
		fmt.Printf("Response code is %s, exiting anyway\n", resp.Status)
		os.Exit(-1)
	}



	return " "
}
