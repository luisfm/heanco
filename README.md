# Heanco: Email Header Analyzer

Heanco is a email message analyzer that extracts MTAs, sender email, builds MTA flows, performs some reputation checks to [abuseipdb](https://www.abuseipdb.com/) and retrieve [SPF](https://tools.ietf.org/html/rfc7208) results from header. There is a tool from Azure that
builds MTAs flow graph, it is [https://mha.azurewebsites.net/](https://mha.azurewebsites.net/). 
We are working on email body extraction for searching phishing URLs, [SPF](https://tools.ietf.org/html/rfc7208) manual check and [DMARC](https://tools.ietf.org/html/rfc7489) retrieving. Samples has been copied from [Demisto Github](https://github.com/demisto/content).
It is important to note that you can run Heanco on Linux, Windows or MacOS 
platforms but you must install [Golang](https://golang.org/dl/).

## Setup

Before download, please create an [abuseipdb](https://www.abuseipdb.com/) account and generate API key. Then, you need to write own key in [conf/api.conf](conf/api.conf) file. Please note that current key is revoked
or field is empty, but feel free to use it if you want to test samples.

## Install

In order to install it, please clone repository. After that, build Heanco
tool as following:

```bash
$ git clone https://gitlab.com/luisfm/heanco
$ go build
```

## Usage

So if you want to use Heanco for building MTA flow graph or MTA IP reputation,
you can test with [samples/*.eml](samples/) files. For instance:

```bash
$ ./heanco scan -s samples/01.eml
[*] Sample file .\samples\01.eml
[*] Scan action has been triggered!
[+] Extracting MTAs from sample header
[!] SPF for IP 54.225.87.32 with MAIL FROM cc-inkyphishfence.com -> pass
[!] SPF for IP 104.47.34.116 with MAIL FROM demisto.com -> pass
[!] SPF for IP 52.54.78.222 with MAIL FROM demisto.com -> pass
[!] Sender name/mail: Rony Kozakish <rony@demisto.com>
[+] Building flow graph of MTAs nodes
        [0] DM5PR11MB1722.namprd11.prod.outlook.com (0.0.0.0) -> DM5PR11MB1722.namprd11.prod.outlook.com (0.0.0.0)
        |
        [1] DM5PR11MB1722.namprd11.prod.outlook.com (0.0.0.0) -> DM5PR11MB0042.namprd11.prod.outlook.com (0.0.0.0)
        |
        [2] NAM01-SN1-obe.outbound.protection.outlook.com (207.46.163.111) -> obc.inkyphishfence.com (52.54.78.222)
        |
        [3] [127.0.0.1] (0.0.0.0) -> obc.inkyphishfence.com (52.54.78.222)
        |
        [4] obc.inkyphishfence.com (52.54.78.222) -> SN1NAM02FT012.mail.protection.outlook.com (0.0.0.0)
        |
        [5] SN1NAM02FT012.eop-nam02.prod.protection.outlook.com (0.0.0.0) -> MWHPR10CA0020.outlook.office365.com (0.0.0.0)
        |
        [6] MWHPR10CA0020.namprd10.prod.outlook.com (0.0.0.0) -> SN2PR10MB0096.namprd10.prod.outlook.com (0.0.0.0)
        |
        [7] NAM01-BY2-obe.outbound.protection.outlook.com (216.32.181.175) -> BL2NAM02FT005.mail.protection.outlook.com (0.0.0.0)
        |
        [8] BL2NAM02FT005.eop-nam02.prod.protection.outlook.com (0.0.0.0) -> DM5PR11CA0013.outlook.office365.com (0.0.0.0)
        |
        [9] DM5PR11CA0013.namprd11.prod.outlook.com (0.0.0.0) -> DM5PR11MB1723.namprd11.prod.outlook.com (0.0.0.0)
        |
        [10] NAM04-SN1-obe.outbound.protection.outlook.com (216.32.180.79) -> ipw.inkyphishfence.com (3.80.166.64)
        |
        [11] ipw.inkyphishfence.com (3.80.166.64) -> BL2NAM02FT054.mail.protection.outlook.com (0.0.0.0)
        |
        [12] BL2NAM02FT054.eop-nam02.prod.protection.outlook.com (0.0.0.0) -> DM3PR11CA0009.outlook.office365.com (0.0.0.0)
        |
        [13] DM3PR11CA0009.namprd11.prod.outlook.com (0.0.0.0) -> DM5PR11MB1723.namprd11.prod.outlook.com (0.0.0.0)
        |
        [14] DM5PR11MB1723.namprd11.prod.outlook.com (0.0.0.0) -> DM5PR11MB1722.namprd11.prod.outlook.com (0.0.0.0)

[*] Checking ipw.inkyphishfence.com (3.80.166.64) reputation
[!] ipw.inkyphishfence.com (3.80.166.64)
        [*] Checking NAM04-SN1-obe.outbound.protection.outlook.com (216.32.180.79) reputation
[!] NAM04-SN1-obe.outbound.protection.outlook.com (216.32.180.79)
        [!] Risk: None (0.000000)
        [!] Risk: None (0.000000)
        [!] Country: US
        [!] Domain: amazon.com
        [!] ISP: Amazon Data Services NoVa
[!] Country: US
        [!] Domain: microsoft.com
        [!] ISP: Microsoft Corporation
[*] Checking obc.inkyphishfence.com (52.54.78.222) reputation
[!] obc.inkyphishfence.com (52.54.78.222)
        [*] Checking NAM01-SN1-obe.outbound.protection.outlook.com (207.46.163.111) reputation
[!] NAM01-SN1-obe.outbound.protection.outlook.com (207.46.163.111)
        [!] Risk: None (0.000000)
        [!] Country: US
        [!] Domain: microsoft.com
        [!] ISP: Microsoft Corporation
[!] Risk: None (0.000000)
        [!] Country: US
        [!] Domain: amazon.com
        [!] ISP: Amazon Technologies Inc.
[*] Checking NAM01-BY2-obe.outbound.protection.outlook.com (216.32.181.175) reputation
[!] NAM01-BY2-obe.outbound.protection.outlook.com (216.32.181.175)
        [!] Risk: None (0.000000)
        [!] Country: US
        [!] Domain: microsoft.com
        [!] ISP: Microsoft Corporation
```

Non-public MTAs has been flagged with 0.0.0.0 IP (yep, could have worn something else).
Another execution example can be seen below:

```bash
$ ./heanco scan -s samples/05.eml
[*] Sample file samples/05.eml
[*] Scan action has been triggered!
[+] Extracting MTAs from sample header
[!] Sender name/mail: SouthTrust Bank <support_num_5713484242@southtrust.com>
[+] Building flow graph of MTAs nodes
        [0] c-24-22-2-133.hsd1.or.comcast.net (24.22.2.133) -> 201.48.40.153 (201.48.40.153)
        |
        [1] 201.48.40.153 (201.48.40.153) -> mail1.domain.com (66.96.162.92)
        |
        [2] mail1.domain.com (66.96.162.92) -> naughty.domain.com (66.96.162.92)

[*] Checking mail1.domain.com (66.96.162.92) reputation
[*] Checking c-24-22-2-133.hsd1.or.comcast.net (24.22.2.133) reputation
[*] Checking 201.48.40.153 (201.48.40.153) reputation
[!] mail1.domain.com (66.96.162.92)
        [!] Risk: None (0.000000)
        [!] Country: US
        [!] Domain: enduranceinternational.com
        [!] ISP: The Endurance International Group Inc.
[!] 201.48.40.153 (201.48.40.153)
        [!] Risk: High (100.000000)
        [!] Country: BR
        [!] Domain: ctbctelecom.com.br
        [!] ISP: Algar Telecom S/A
[!] c-24-22-2-133.hsd1.or.comcast.net (24.22.2.133)
        [!] Risk: None (0.000000)
        [!] Country: US
        [!] Domain: comcast.net
        [!] ISP: Comcast Cable Communications LLC
```

Finally, this project has been implemented in order to learn Golang therefore
will be bugs. You can report me as issue or via Twitter [@lfm3773](https://twitter.com/lfm3773).
