module heanco

go 1.13

require (
	github.com/AlienVault-OTX/OTX-Go-SDK v0.0.0-20160304163638-ac284b780774
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/google/go-querystring v1.0.0 // indirect
	github.com/hedzr/cmdr v1.6.47
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
)
